# Features
- Search bar moves with you as you scroll
- Search auto-focuses every time you return to the page
- Press [enter] to focus the search bar
- Prevents search if you press [enter] when there is no text in the search bar

## Notes
- Not endorsed by Gamepedia
- [enter] will perform it's normal action if a button or link is focused.
- This is a first version and hobby project, so is not super well polished.

## Well-supported pedias
These Gamepedias have been tested & still look reasonably good. It will work well on most Gamepedia wikis. Contact me if you'd like to get support.
- World of Warcraft
- League of Legends
- Minecraft
- DOTA 2
- SMITE
- Feed The Beast
- No Man's Sky
- Zelda
- Fortnite

## Contact
You can contact me at:
- [@TaelufDev on Twitter](https://twitter.com/TaelufDev) (near-daily checkin)
- [contact@taeluf.com](mailto:contact@taeluf.com) (~weekly checkin)
- [@Taeluf on Facebook](https://facebook.com/Taeluf) (not very responsive)

Contact me for:
- Feature suggestions
- Complaints / bug reports / accessibility issues
- Other feedback
- Contribute money or code or art
- Whatever else

## The code
The source code is available to view & contribute to, but not to use. View it on [Github](https://github.com/Taeluf/Gamepedia-Search)