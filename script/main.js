if (typeof browser === 'undefined') {
    var browser = chrome;
}

function focusSearch() {
    const searchInput = document.getElementById("searchInput");
    searchInput.focus();
}

function loadStyle(href) {
    for (var i = 0; i < document.styleSheets.length; i++) {
        if (document.styleSheets[i].href == href) {
            return;
        }
    }
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);
}

function handleKey(event){
    console.log('key:'+event.keyCode);
    console.log(document.activeElement);
    if (event.keyCode===13
        &&document.activeElement.tagName.toUpperCase()!='A'
        &&document.activeElement.tagName.toUpperCase()!='BUTTON'
        &&document.activeElement!=document.getElementById("searchInput")){
            focusSearch();
            event.preventDefault();
            event.stopPropagation();
        }
    else if (event.keyCode==13
        &&document.activeElement==document.getElementById("searchInput")
        &&document.getElementById("searchInput").value==''){
            event.preventDefault();
            event.stopPropagation();
        }
}

function doSetup() {
    const cssUrl = browser.runtime.getURL("/style/main.css");
    loadStyle(cssUrl);
    focusSearch();
    if (document.getElementById("taeluf-gamepedia-mod")==null){
        const a = document.createElement('a');
        a.id = "taeluf-gamepedia-mod";
        a.href = '/index.php?title=Special:Search&profile=advanced&search=&fulltext=1';
        a.innerText = "Advanced Search";
        const nav = document.getElementById("mw-head");
        nav.insertBefore(a,nav.children[0]);
    }
}
window.addEventListener("focus", focusSearch);
document.body.addEventListener("load", doSetup);
document.body.addEventListener('keydown', handleKey);

doSetup();